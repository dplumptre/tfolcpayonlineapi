<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/getinfo', 'ApiController@getInfo');


Route::post('/dollar-payment', 'PaystackController@goToGateway')->name('ps.post.dp');
Route::post('/webhook', 'PaystackController@webHook')->name('ps.webhook');
Route::get('/payment/callback/', 'PaystackController@handleGatewayCallback')->name('ps.callback');


Route::post('/local-payment', 'GlobalpayController@saveDetails')->name('gp.sd');
Route::get('/global-callback', 'GlobalpayController@globalpayCallback')->name('gp.post');