<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResponseEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;

    public function __construct($value)
    {
        $this->data = $value;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      
        return $this->view('mails.responseMail')
        ->with([
            'name' => $this->data['name'],
            'amount' => $this->data['amount'],
            'result' => $this->data['result'],
            'purpose' => $this->data['purpose'],
            'currency' => $this->data['currency'],
        ]);
    }
}


