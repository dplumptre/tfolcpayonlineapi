<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Paystack;
use App\Transaction;
use Illuminate\Support\Facades\Storage;
use App\Mail\ResponseEmail;
use Illuminate\Support\Facades\Mail;

class PaystackController extends Controller
{
    
    use Paystack;

    


    public function goToGateway(Request $request){

        $user = Transaction::create([
            'fullname'           =>$request->fullname,
            'email'              =>$request->email,
            'mobile'             =>$request->mobile,
            'purpose'            =>$request->purpose,
            'currency'           =>'USD',
            'amount'             =>$request->amount,
            'vendor'             =>'paystack',
            'status'             =>'processing',
            'merchant_ref'       => $this->generateRandomString(),
        ]);
       $tranx =  $this->sendToGateway($request->amount,$request->email,"USD");
       // this sends back the url for your payment where you inser your card
       // from the frontend you use this link to navigate
       //dd($tranx['data']['authorization_url']);
       return response()->json(['url'=>$tranx['data']['authorization_url'] ],201);
    }








    public function handleGatewayCallback(Request $request){
    
        
        $response = $this->doCallback($request->input('reference'));
        $tranx = json_decode($response);
        
       if(!$tranx->status){die('API returned error: ' . $tranx->message); }// there was an error from the API
    
      if('success' == $tranx->data->status){
         // $amount = $tranx->data->amount /100;
          //try{
          //   $p = Transaction::where('email',$event->customer->email)
          //   ->where('vendor','paystack')
          //   ->where('amount',$event->data->amount)->latest('id')->first();
          // }catch(Exception $e){
          //   return response()->json(['error'=>$e->getMessage()],400);
          // }
          // if(!$p ){return response()->json(
          //   ['error'=>"an error occured, make sure you use same email used in registering"]
          //   ,400
          //   );
          // }
          // redirect to success page
          header("Location: ".$this->redirect_link.'success');
          die();
        }else{
          header("Location: ".$this->redirect_link.'fail');
          die();
        }



    
    }
    




    public function webHook(Request $request){
     
      // if ((strtoupper($_SERVER['REQUEST_METHOD']) != 'POST' ) || !array_key_exists('x-paystack-signature', $_SERVER) ) 
      //exit();
     // Retrieve the request's body
      $input = @file_get_contents("php://input");
      define('PAYSTACK_SECRET_KEY',env('PAYSTACK_SECRET_KEY', false));
    // validate event do all at once to avoid timing attack
      if($_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] !== hash_hmac('sha512', $input, PAYSTACK_SECRET_KEY))
        exit();
      http_response_code(200);
      $event = json_decode($input);

      if( $event->event ==  'charge.success'){

        $amount = $event->data->amount/100;

        $u = Transaction::where('email',$event->data->customer->email)
                            ->where('vendor','paystack')
                            ->where('amount',$amount)->latest('id')->first();
        $u->status = 'completed';
        $u->save();    

     // send email
        $data = [
          'name'     => $u->fullname,
          'amount'   => $u->amount,
          'result'   => 'was successful',
          'purpose'  => $u->purpose,
          'currency' => $u->currency,
        ];
       
       Mail::to($u->email)->send(new ResponseEmail($data));
          
      }
    
    
    








        
    
    
    }
    
    
    
    
    
        public function test()
        {
    
        $p = Paystack::genTranxRef() ;
    
        return $p;
    
        }








}
