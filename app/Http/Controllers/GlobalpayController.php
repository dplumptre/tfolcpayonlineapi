<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\globalpay\GlobalPay;
use App\Transaction;
use App\Mail\ResponseEmail;
use Illuminate\Support\Facades\Mail;

ini_set ('soap.wsdl_cache_enabled', 0);
include(app_path() . '/Traits/globalpay/nusoap.php');
//include 'nusoap.php'
use nusoap_client;

class GlobalpayController extends Controller
{

 use GlobalPay;




 public function saveDetails(Request $request){

    $user = Transaction::create([
        'fullname'           =>$request->fullname,
        'email'              =>$request->email,
        'mobile'             =>$request->mobile,
        'purpose'            =>$request->purpose,
        'currency'           =>'NGN',
        'amount'             =>$request->amount, 
        'vendor'             =>'globalpay',
        'status'             =>'processing',
        'merchant_ref'       => $request->merchant_ref,
    ]);

    return response()->json(['response'=>"success" ],200);
}   





 public function globalpayCallback(Request $request){
     

    
    $client = new nusoap_client( $this->nusoap_client, true);
    
    $soapaction = $this->soapaction;
    $namespace = $this->namespace;
    $client->soap_defencoding = 'UTF-8';

    $status = $request->input('status');	
    $txnref = $request->input('txnref');   
    $merch_txnref=$txnref; 
    $channel="" ; 
    $merchantID= $this->merchantID; 
    $start_date=""; 
    $end_date=""; 
    $uid="fo_ws_user"; 
    $pwd="fo_ws_password"; 
    $payment_status="" ; 

    $err = $client->getError();
    if ($err) { die('<h2>Constructor error</h2><pre>' . $err . '</pre>'); }

    $MethodToCall= "getTransactions";

    $param = array(
        'merch_txnref' => $merch_txnref, 
        'channel' => $channel,
        'merchantID' => $merchantID,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'uid' => $uid,
        'pwd' => $pwd,
        'payment_status' => $payment_status
    );

    $result = $client->call(
        'getTransactions', 
        array('parameters' => $param), 
        'https://www.eazypaynigeria.com/globalpay/', 
        'https://www.eazypaynigeria.com/globalpay/getTransactions', 
        false,  
        true
    );


    // Check for a fault
    if ($client->fault) {
        echo '<h2>Fault</h2><pre>';
        print_r($result);
        echo '</pre>';
        return $result;
    }else {
    // Check for errors
    $err = $client->getError();





    if ($err) {die( '<h2>Error</h2><pre>' . $err . '</pre>');
    }else {
        //This gives getTransactionsResult
        $WebResult=$MethodToCall."Result";
        // Pass the result into XML
        $xml = simplexml_load_string($result[$WebResult]);

       // dd($xml);
        $amount = $xml->record->amount;
        $txn_date = $xml->record->payment_date;
        $pmt_method = $xml->record->channel;
        $pmt_status = $xml->record->payment_status;
        $pmt_txnref = $xml->record->txnref;
        $currency = $xml->record->field_values->field_values->field[2]->currency;
        $trans_status = $xml->record->payment_status_description;
   } 






   $amount = floatval($amount); // removing .oo from end of amount

    $u = Transaction::where('merchant_ref',$merch_txnref)
        ->where('vendor','globalpay')
        ->where('amount',$amount)->latest('id')->first();
    $u->status = 'completed';
    $u->save();
    


    if ($pmt_status == 'successful' && $amount == $u->amount ){



            $data = [
                'name'     => $u->fullname,
                'amount'   => $u->amount,
                'result'   => 'was successful',
                'purpose'  => $u->purpose,
                'currency' => $u->currency,
              ];


            // send email
            Mail::to($u->email)->send(new ResponseEmail($data));
            header("Location: ".$this->redirect_link.'success');
    }else{
        $data = [
            'name'     => $u->fullname,
            'amount'   => $u->amount,
            'result'   => 'failed',
            'purpose'  => $u->purpose,
            'currency' => $u->currency,
          ];
            // send email
            Mail::to($u->email)->send(new ResponseEmail($data));
            header("Location: ".$this->redirect_link.'fail');

    }


    } //cleient fault end


 }






}
